/**
 * Extension's Javascript
 * @ingroup Extensions
 * @author Josef Martiňák
 */

( function ( mw, $ ) {

     // Finds text "-NEW-SIMILAR-TITLES-" on edit pages and replaces it with popup window
     // Window shows similar articles
	var vars = getUrlVars();
	if( vars['action'] == 'edit') {

		editorElem = $("#wpTextbox1");

		// it's edit page. Now get content of textbox
		var content = editorElem.val();
		if( content.indexOf( '-NEW-SIMILAR-TITLES-' ) != -1 ) {
			// there are some similar pages listed - read them
			var arr = content.match(/[^\r\n]+/g);
			var wikipath = window.location.origin + "/index.php";
			var links = '';
			for ( var i = 0; i < ( arr.length-1 ); i++ ) {
				var brr = arr[i].split( ' ', 2 );
				links += "<a href='" + wikipath + "?curid=" + brr[0] + "' target='_blank'>";
				links += brr[1].replace( '_', ' ' ) + "</a><br/>";
			}
			var position = editorElem.position();
			editorElem.val( '' );
			// create box
			editorElem.after( "<div id='SimilarTitlesPopup'></div>" );
			$( '#SimilarTitlesPopup' ).css( 'left', position.left + 20 );
			$( '#SimilarTitlesPopup' ).css( 'top', position.top + 100 );
			// insert cancel button (and onclick event)
			$( '#SimilarTitlesPopup' ).append( "<div id='SimilarTitlesCancelButton'>" + mw.message( 'similartitles-close' ).plain() + "</div>" );
			// insert header
			$( '#SimilarTitlesPopup' ).append( "<p id='SimilarTitlesHeader'>" + mw.message( 'similartitles-header' ).plain() );
			// insert links
			$( '#SimilarTitlesPopup' ).append( links + '<br/>' );
			// cancel
			$( '#SimilarTitlesCancelButton' ).click(function() {
				$( '#SimilarTitlesPopup' ).animate({
					opacity: '0'
				}, 300, function() {
					// Animation complete
					$( '#SimilarTitlesCancelButton' ).off('click');
					$( '#SimilarTitlesPopup' ).remove();
					editorElem.focus();
				});
			});
		}
	}

}( mediaWiki, jQuery ) );


/**
 * Read a page's GET URL variables and return them as an associative array.
 * @return array: part of current URL
 */
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        var hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
