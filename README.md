# SimilarTitles

Mediawiki extension.

## Description

* Version 1.2.1
* Extension offers pages with similar names when creating new page.
* Can help avoiding duplicities.

## Installation

* Make sure you have MediaWiki 1.36+ installed.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'SimilarTitles' )`;

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## RELEASE NOTES

### 1.2

* Fix for Visual Editor

### 1.2.1

* "The constant DB_SLAVE/MASTER deprecated in 1.28. Use DB_REPLICA/PRIMARY instead.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2023 First Faculty of Medicine, Charles University