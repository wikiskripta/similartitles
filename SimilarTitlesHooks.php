<?php

/**
 * All hooked functions used by SimilarTitles
 * @ingroup Extensions
 * @author Josef Martiňák
 */

class SimilarTitlesHooks {

	/**
	 * Find and display articles with similar titles
	 * Puts similar articles' names with pageid to edit window
	 * @param string $text: text to prefill edit form with
	 * @param object $title: title of new page (instance)
	 */
	public static function findSimilarArticles( &$text, &$title ) {

		$context = new RequestContext();
		$qvalues = $context->getRequest()->getQueryValues();
		if(array_key_exists( "action", $qvalues ) && $qvalues["action"] == "edit") $wikieditor = true;
		else $wikieditor = false;

		if( !$wikieditor || $title->getNamespace() != 0 ) {
			return true;
		}

		$newtitle = $title->getText();

		// get all wiki articles from NS0
		$dbr = wfGetDB(DB_REPLICA);
		$res = $dbr->select(
			'page',
			array( 'page_id', 'page_title' ),
			array( 'page_namespace' => 0 ),
			"__METHOD__",
			array( 'ORDER BY' => 'page_title' )
		);

		foreach( $res as $row ) {
			similar_text( $newtitle, $row->page_title, $percent);
			if( $percent > 60 ) {
				$text .= $row->page_id . ' ' . $row->page_title . PHP_EOL;
			}
		}

		if( !empty( $text ) ) {
			$text .= '-NEW-SIMILAR-TITLES-';
		}

    	return true;
	}

	/**
	 * Add resources
	 * @param $out OutputPage: instance of OutputPage
	 * @param $skin Skin: instance of Skin
	 */
	public static function createPopup( &$out, &$skin ) {

		$title = $out->getTitle();
		$qvalues = $out->getRequest()->getQueryValues();
		$wikieditor = false;
		if(array_key_exists( "action", $qvalues ) && $qvalues["action"] == "edit") $wikieditor = true;
		if( $title->getNamespace() == 0 && !empty( $qvalues ) && $wikieditor ) {
			$out->addModules('ext.SimilarTitles');
		}

		return true;
	}
}